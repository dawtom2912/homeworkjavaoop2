package Workers;

import java.util.ArrayList;
import java.util.List;
import Workers.Company;         //inaczej mi nie chciało działać żebym miał listę w klasie Manager, dałem ją public,
                                //  nie poszło, dałem private z getter'em, nie poszło bo chciało static, a jak był static
                                    //  to znowu coś się sypało, więc zrobiłem tak, nie wiem czy to najlepsza opcja

/**
 * Created by Admin on 2016-04-07.
 */
public class Manager extends Employee {


    private Type type;

    private double budget;
    private int count;

    enum Type {
        hireBySomeFixedBudget, hireByCountLimit
    }

    public boolean canIHireByBudget(Employee employee, double budget) {
        double sum = 0;

        for (Employee current : Company.employees) {
            sum += current.getSalary();
        }
        if (sum + employee.getSalary() > budget)
            return false;
        else
            return true;
    }

    public boolean canIHireByCount(Employee employee, int count) {
        if (Company.employees.size() >= count)
            return false;
        else
            return true;
    }

    public boolean canIHire(Type type, Employee employee, Manager manager) {
        if (manager.type == Type.hireBySomeFixedBudget)
            return canIHireByBudget(employee, budget);
        else if (manager.type == Type.hireByCountLimit)
            return canIHireByCount(employee, count);
        else
            return false;           //rzucał się jak nie było else'a, że nie zwracam wartości
    }

    public void hire(Employee employee, List<Employee> list, Manager manager) {
        if (canIHire(manager.type, employee, manager)) {
            list.add(employee);
        }
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    public Manager(String name, double salary, Type type) {
        super(name, salary);
        this.type = type;
        if (salary > 10000) this.isSatisfied = true;
        else this.isSatisfied = false;
    }
}

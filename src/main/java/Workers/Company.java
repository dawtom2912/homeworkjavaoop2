package Workers;

import java.util.List;

/**
 * Created by Admin on 2016-04-09.
 */
public class Company {
    private static Company ourInstance = new Company();

    public static Company getInstance() {
        return ourInstance;
    }

    private Company() {
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public static List<Employee> employees;

    void hireCEO(CEO ceo) {
        employees.add(0, ceo);
    }

    @Override
    public String toString() {
        String result = "";
        for (Employee current : employees) {
            if (current.getClass().equals("Manager")){
                result = result + "\t";
            }
            if (current.getClass().equals("Employee")){
                result = result + "\t\t";
            }
            result = result + current.getName() + " - " + current.getClass() + "\n";
        }
        return result;
    }

    void displayTheCompany(List<Employee> employees) {
        System.out.println(toString());
        }
}

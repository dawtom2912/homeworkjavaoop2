package Workers;

/**
 * Created by Admin on 2016-04-06.
 */

public class Employee {
    protected String name;
    protected double salary;
    boolean isSatisfied;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(double salary) {
        this.salary = salary;
        if (salary > 10000) this.isSatisfied = true;
        else this.isSatisfied = false;
    }

    public double getSalary() {
        return salary;
    }

    public boolean isSatisfied() {
        return isSatisfied;
    }

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
        if (salary > 10000) this.isSatisfied = true;
        else this.isSatisfied = false;
    }
}

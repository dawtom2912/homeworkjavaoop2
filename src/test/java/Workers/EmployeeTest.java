package Workers;

//Przepraszam za ten kod, ale jakoś nie mogę ogarnąć testowania, dotyczy testów

import org.junit.Test;
import org.junit.Before;


import static org.junit.Assert.*;

/**
 * Created by Admin on 2016-04-09.
 */
public class EmployeeTest {

    Employee employee = new Employee("Example name", 0);

    @Test
    public void SatisfactionTestPositive(Employee employee) throws Exception {
        employee.setSalary(15000);
        assertTrue(employee.isSatisfied);
    }
    public void SatisfactionTestNegative(Employee employee) throws Exception {
        employee.setSalary(5000);
        assertFalse(employee.isSatisfied);
    }


}